# Ludum Dare 44 project

## Git rules

* No commits to `master` branch
* Feature commits go to `feature/JIRA-TICKET-ID` or `feature/short-description`
* Bugfix commits go to `bugfix/JIRA-TICKET-ID` or `bugfix/short-description`
* Everything is merged through pull requests
* All commit messages should contain the text `JIRA-TICKET-ID`