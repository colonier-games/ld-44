package games.colonier.ld44.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import games.colonier.ld44.LD44Game;

public class DesktopLauncher {

	public static void main (String[] arg) {

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 1280;
		config.height = 720;
		config.fullscreen = false;
		config.vSyncEnabled = true;
		config.foregroundFPS = 0;
		config.backgroundFPS = 0;
		config.samples = 4;
		config.resizable = false;
		config.addIcon("chemistree-icon.png", Files.FileType.Internal);
		config.title = "ChemisTree";

		new LwjglApplication(new LD44Game(), config);

	}

}
