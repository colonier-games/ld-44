package games.colonier.ld44.logic;

import javax.imageio.ImageIO;
import java.util.Arrays;
import java.util.List;

public class Difficulty {

    private static final Difficulty Easy = new Difficulty (
            "Easy",
            1,
            1000,
            0.1f,
            0.1f
    );
    private static final Difficulty Normal = new Difficulty (
            "Normal",
            2,
            100,
            0.1f,
            0.1f
    );
    private static final Difficulty Hard = new Difficulty (
            "Hard",
            3,
            100,
            0.25f,
            0.25f
    );
    private static final Difficulty Impossible = new Difficulty (
            "Impossible",
            4,
            50,
            0.4f,
            0.4f
    );

    public static final List <Difficulty> allDifficulties = Arrays.asList (
            Easy,
            Normal,
            Hard,
            Impossible
    );

    private static Difficulty chosen = Easy;

    public final String name;
    public final int ruleComplexity;
    public final int numTestParticipants;
    public final float nonexistentSymptomProbability;
    public final float undiscoveredSymptomProbability;

    public Difficulty (
            String name,
            int ruleComplexity,
            int numTestParticipants,
            float nonexistentSymptomProbability,
            float undiscoveredSymptomProbability
    ) {
        this.name = name;
        this.ruleComplexity = ruleComplexity;
        this.numTestParticipants = numTestParticipants;
        this.nonexistentSymptomProbability = nonexistentSymptomProbability;
        this.undiscoveredSymptomProbability = undiscoveredSymptomProbability;
    }

    public static Difficulty getChosen () {
        return chosen;
    }

    public static void setChosen (Difficulty chosen) {
        Difficulty.chosen = chosen;
    }

    public static Difficulty increase () {
        if (chosen == Easy) return chosen = Normal;
        if (chosen == Normal) return chosen = Hard;
        if (chosen == Hard) return chosen = Impossible;
        return chosen = Easy;
    }

}
