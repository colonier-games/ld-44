package games.colonier.ld44.logic;

import games.colonier.ld44.chemistry.Molecule;
import games.colonier.ld44.chemistry.Symptom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCase implements Comparable<TestCase> {

    public float predictedSeverity;
    public Map <Symptom, Integer> simulationSymptoms = new HashMap<> ();
    public boolean canCureDisease;
    public int severity;
    public List <Symptom> symptoms = new ArrayList<> ();
    public boolean isSuccessful;
    public Molecule reactedDiseaseMolecule;
    public List <AnalysisItem> analysisItems = new ArrayList<> ();

    @Override
    public int compareTo(TestCase o) {
        if (this.isSuccessful && !o.isSuccessful) return -1;
        if (!this.isSuccessful && o.isSuccessful) return 1;
        return severity - o.severity;
    }
}
