package games.colonier.ld44.logic;

import games.colonier.ld44.chemistry.Atom;
import games.colonier.ld44.chemistry.Symptom;

import java.util.List;
import java.util.Objects;

public class AnalysisItem {

    public final List <Atom> atoms;
    public final Symptom symptom;

    public AnalysisItem (List<Atom> atoms, Symptom symptom) {
        this.atoms = atoms;
        this.symptom = symptom;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass () != o.getClass ()) {
            return false;
        }
        AnalysisItem item = (AnalysisItem) o;
        return Objects.equals (atoms, item.atoms) &&
                symptom == item.symptom;
    }

    @Override
    public int hashCode () {
        return Objects.hash (atoms, symptom);
    }

}
