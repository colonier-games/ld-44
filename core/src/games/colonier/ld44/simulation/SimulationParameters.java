package games.colonier.ld44.simulation;

public class SimulationParameters {

    public final int numTestParticipants;
    public final float nonexistentSymptomProbability;
    public final float existentSymptomSkipProbability;

    public SimulationParameters (
            int numTestParticipants,
            float nonexistentSymptomProbability,
            float existentSymptomSkipProbability
    ) {
        this.numTestParticipants = numTestParticipants;
        this.nonexistentSymptomProbability = nonexistentSymptomProbability;
        this.existentSymptomSkipProbability = existentSymptomSkipProbability;
    }

}
