package games.colonier.ld44.simulation;

import games.colonier.ld44.chemistry.Symptom;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SimulationParticipant {

    public final int id;
    public Map <Symptom, Boolean> symptomMap = new HashMap<> ();

    public SimulationParticipant (int id) {
        this.id = id;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass () != o.getClass ()) {
            return false;
        }
        SimulationParticipant that = (SimulationParticipant) o;
        return id == that.id;
    }

    @Override
    public int hashCode () {
        return Objects.hash (id);
    }

}
