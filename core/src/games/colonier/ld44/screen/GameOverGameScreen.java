package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.Colors;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.logic.GameState;
import games.colonier.ld44.text.Fonts;

public class GameOverGameScreen extends AbstractGameScreen implements InputProcessor {

    private boolean menu;
    private float increm = 0.0f;

    public GameOverGameScreen (LudumGame ludumGame) {
        super (ludumGame);
    }

    @Override
    protected void onInit () {

        Gdx.input.setInputProcessor (this);

    }

    @Override
    protected void onResize (float w, float h) {

    }

    @Override
    protected void update (float delta) {
        increm = (float)(Math.sin(getScreenTime()) * 30.0f);
    }

    @Override
    protected void onDispose () {

    }

    private void renderWin () {

        spriteBatch.begin ();

        Fonts.f128 ().draw (
                spriteBatch,
                "Congratulations!",
                32f, resolution.y / 2.0f + 150.0f + increm,
                resolution.x - 64,
                Align.center, true
        );

        Fonts.f64().draw(
                spriteBatch,
                "Press any button to return to the menu",
                32f,resolution.y / 2.0f + 150.0f - 130.0f + increm,
                resolution.x,
                Align.center, true
        );

        spriteBatch.end ();

    }

    @Override
    public void render (float delta) {

        Gdx.gl.glClearColor (
                Colors.Background.r,
                Colors.Background.g,
                Colors.Background.b,
                Colors.Background.a
        );
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT | Gdx.gl.GL_DEPTH_BUFFER_BIT);

        if (!menu) {
            if (GameState.getInstance ().getLast () != null) {
                if (GameState.getInstance ().getLast ().isSuccessful) {
                    renderWin ();
                }
            }
        }

    }

    @Override
    public GameScreen next () {
        if(menu) {
            if (!GameState.getInstance().isFirstRun()) {
                return new MenuGameScreen(ludumGame);
            } else {
                return new CreditGameScreen(ludumGame);
            }
        }
        return null;
    }

    @Override
    public boolean keyDown (int keycode) {
        return false;
    }

    @Override
    public boolean keyUp (int keycode) {
        GameState.getInstance ().load ();
        menu = true;
        return false;
    }

    @Override
    public boolean keyTyped (char character) {
        return false;
    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp (int screenX, int screenY, int pointer, int button) {
        GameState.getInstance ().load ();
        menu = true;
        return false;
    }

    @Override
    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved (int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        return false;
    }

}
