package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.renderer.RoundedShapeRenderer;

public abstract class AbstractGameScreen implements GameScreen {

    protected final LudumGame ludumGame;

    protected AssetManager assetManager;
    protected SpriteBatch spriteBatch;
    protected RoundedShapeRenderer shapeRenderer;

    protected Vector2 resolution = new Vector2 ();

    protected float screenTime = 0.0f;
    protected float deltaTime = 0.0f;

    protected Vector2 mouse = new Vector2 ();

    public AbstractGameScreen (LudumGame ludumGame) {
        this.ludumGame = ludumGame;
    }

    protected abstract void onInit ();

    @Override
    public void init () {

        spriteBatch = new SpriteBatch ();
        shapeRenderer = new RoundedShapeRenderer ();
        assetManager = new AssetManager ();

        resize (Gdx.graphics.getWidth (), Gdx.graphics.getHeight ());

        this.onInit ();

    }

    protected abstract void onResize (float w, float h);

    @Override
    public void resize (float w, float h) {

        this.resolution.set (w, h);
        this.onResize (w, h);

    }

    @Override
    public Vector2 getResolution () {
        return resolution;
    }

    protected abstract void update (float delta);

    @Override
    public void tick (float delta) {

        this.deltaTime = delta;
        this.screenTime += delta;

        update (delta);

    }

    @Override
    public float getGameTime () {
        return ludumGame.getAppTime ();
    }

    @Override
    public float getDelta () {
        return deltaTime;
    }

    @Override
    public float getScreenTime () {
        return screenTime;
    }

    @Override
    public AssetManager getAssetManager () {
        return assetManager;
    }

    @Override
    public SpriteBatch getSpriteBatch () {
        return spriteBatch;
    }

    @Override
    public ShapeRenderer getShapeRenderer () {
        return shapeRenderer;
    }

    protected Vector2 screenToMouse (float sx, float sy) {
        return mouse.set (sx, resolution.y - sy);
    }

    protected abstract void onDispose ();

    @Override
    public void dispose () {

        spriteBatch.dispose ();
        shapeRenderer.dispose ();
        assetManager.dispose ();

        onDispose ();

    }

}
