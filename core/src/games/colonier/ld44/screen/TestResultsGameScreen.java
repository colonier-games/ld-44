package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.Colors;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.chemistry.Symptom;
import games.colonier.ld44.logic.GameState;
import games.colonier.ld44.logic.TestCase;
import games.colonier.ld44.text.Fonts;
import games.colonier.ld44.util.ColorUtil;

public class TestResultsGameScreen extends AbstractGameScreen implements InputProcessor {

    private TestCase testCase;

    private Rectangle backgroundPanelRect = new Rectangle ();
    private Rectangle backgroundPanelShadowRect = new Rectangle ();
    private Rectangle backgroundPanelInsideRect = new Rectangle ();
    private Rectangle resultsPanelRect = new Rectangle ();
    private Rectangle resultsPanelInsideRect = new Rectangle ();

    private Rectangle releaseButtonRect = new Rectangle ();
    private Rectangle analyzeButtonRect = new Rectangle ();
    private Rectangle backButtonRect = new Rectangle ();

    private boolean back, released, release, analyze, escape;

    Camera camera;

    public TestResultsGameScreen (LudumGame ludumGame) {
        super (ludumGame);
    }

    @Override
    protected void onInit () {

        GameState.getInstance ().executeTest ();
        testCase = GameState.getInstance ().getLastSimulated ();
        Gdx.input.setInputProcessor (this);

    }

    @Override
    protected void onResize (float w, float h) {

        camera = new OrthographicCamera (w, h);
        camera.position.set (w / 2.0f, h / 2.0f, 0.0f);
        camera.update ();

    }

    @Override
    protected void update (float delta) {

    }

    @Override
    protected void onDispose () {

    }

    @Override
    public void render (float delta) {

        Gdx.gl.glClearColor (
                Colors.Background.r,
                Colors.Background.g,
                Colors.Background.b,
                Colors.Background.a
        );
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT | Gdx.gl.GL_DEPTH_BUFFER_BIT);

        backgroundPanelRect.set (
                32f, 32f,
                resolution.x - 64f, resolution.y - 64f
        );
        backgroundPanelShadowRect.set (backgroundPanelRect)
                .setX (backgroundPanelRect.x + 8f).setY (backgroundPanelRect.y - 8f);

        backgroundPanelInsideRect.set (
                backgroundPanelRect.x + 16f,
                backgroundPanelRect.y + 16f,
                backgroundPanelRect.width - 32f,
                backgroundPanelRect.height - 32f
        );

        backButtonRect.set (
                backgroundPanelInsideRect.x,
                backgroundPanelInsideRect.y,
                backgroundPanelInsideRect.width * 0.3f,
                192f
        );
        analyzeButtonRect.set (
                backgroundPanelInsideRect.x + backgroundPanelInsideRect.width * 0.35f,
                backgroundPanelInsideRect.y,
                backgroundPanelInsideRect.width * 0.3f,
                192f
        );
        releaseButtonRect.set (
                backgroundPanelInsideRect.x + backgroundPanelInsideRect.width - backgroundPanelInsideRect.width * 0.3f,
                backgroundPanelInsideRect.y,
                backgroundPanelInsideRect.width * 0.3f,
                192f
        );

        resultsPanelRect.set (
                backgroundPanelInsideRect.x,
                backgroundPanelInsideRect.y + backButtonRect.height + 16f,
                backgroundPanelInsideRect.width,
                backgroundPanelInsideRect.height - backButtonRect.height - 16f
        );
        resultsPanelInsideRect.set (
                resultsPanelRect.x + 16f,
                resultsPanelRect.y + 16f,
                resultsPanelRect.width - 32f,
                resultsPanelRect.height - 32f
        );

        StringBuilder sb = new StringBuilder ();

        sb.append ("Molecule can cure disease: ")
                .append (
                        testCase.canCureDisease
                                ? (ColorUtil.markup (Colors.Green) + "Yes" + ColorUtil.markup (Colors.TextDark))
                                : (ColorUtil.markup (Colors.AtomRed) + "No" + ColorUtil.markup (Colors.TextDark))
                ).append ("\n");
        sb.append ("Predicted severity: ")
                .append (
                        testCase.predictedSeverity <= 2f
                                ? ColorUtil.markup (Colors.Green)
                                : testCase.predictedSeverity <= 6f
                                        ? ColorUtil.markup (Colors.Orange)
                                        : ColorUtil.markup (Colors.AtomRed)
                ).append (testCase.predictedSeverity)
                .append (ColorUtil.markup (Colors.TextDark))
                .append ("\n");
        sb.append ("Participants experienced the following symptoms: \n");
        for (Symptom s : testCase.simulationSymptoms.keySet ()) {

            sb.append (s.name ())
                    .append (": ")
                    .append (testCase.simulationSymptoms.get (s)).append (" people\n");

        }

        shapeRenderer.setProjectionMatrix (camera.combined);
        shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor (Colors.LightPanelShadow);
        shapeRenderer.rect (
                backgroundPanelShadowRect.x, backgroundPanelShadowRect.y,
                backgroundPanelShadowRect.width, backgroundPanelShadowRect.height
        );

        shapeRenderer.setColor (Colors.LightPanelBackground);
        shapeRenderer.rect (
                backgroundPanelRect.x, backgroundPanelRect.y,
                backgroundPanelRect.width, backgroundPanelRect.height
        );


        shapeRenderer.setColor (Colors.DarkGray);
        shapeRenderer.rect (
                backButtonRect.x, backButtonRect.y,
                backButtonRect.width, backButtonRect.height
        );

        shapeRenderer.setColor (Colors.Green);
        if (released && !GameState.getInstance ().getLast ().isSuccessful) {
            shapeRenderer.setColor (Colors.AtomRed);
        }
        shapeRenderer.rect (
                releaseButtonRect.x, releaseButtonRect.y,
                releaseButtonRect.width, releaseButtonRect.height
        );


        shapeRenderer.setColor (Colors.AtomBlue);
        shapeRenderer.rect (
                analyzeButtonRect.x, analyzeButtonRect.y,
                analyzeButtonRect.width, analyzeButtonRect.height
        );


        shapeRenderer.setColor (Colors.BluePanelBackground);
        shapeRenderer.rect (
                resultsPanelRect.x, resultsPanelRect.y,
                resultsPanelRect.width, resultsPanelRect.height
        );

        shapeRenderer.end ();

        spriteBatch.setProjectionMatrix (camera.combined);
        spriteBatch.begin ();

        Fonts.f32 ().setColor (Colors.TextDark);
        GlyphLayout gl = Fonts.f32 ().draw (
                spriteBatch,
                "Test results",
                resultsPanelInsideRect.x,
                resultsPanelInsideRect.y + resultsPanelInsideRect.height,
                resultsPanelInsideRect.width,
                Align.center,
                true
        );

        Fonts.f18 ().setColor (Colors.TextDark);
        Fonts.f18 ()
                .draw (
                        spriteBatch,
                        sb.toString (),
                        resultsPanelInsideRect.x,
                        resultsPanelInsideRect.y + resultsPanelInsideRect.height - gl.height - 16f,
                        resultsPanelInsideRect.width,
                        Align.topLeft,
                        true
                );

        Fonts.f32 ().setColor (Colors.TextLight);
        Fonts.f32 ()
                .draw (
                        spriteBatch,
                        "Back",
                        backButtonRect.x,
                        backButtonRect.y + backButtonRect.height / 2f,
                        backButtonRect.width,
                        Align.center,
                        true
                );

        Fonts.f32 ().setColor (Colors.TextLight);
        Fonts.f32 ()
                .draw (
                        spriteBatch,
                        "Release to market",
                        releaseButtonRect.x,
                        releaseButtonRect.y + releaseButtonRect.height / 2f,
                        releaseButtonRect.width,
                        Align.center,
                        true
                );

        Fonts.f32 ().setColor (Colors.TextLight);
        Fonts.f32 ()
                .draw (
                        spriteBatch,
                        "Analyze",
                        analyzeButtonRect.x,
                        analyzeButtonRect.y + analyzeButtonRect.height / 2f,
                        analyzeButtonRect.width,
                        Align.center,
                        true
                );

        spriteBatch.end ();

    }

    @Override
    public GameScreen next () {
        if (back) {
            return new MoleculeEditorGameScreen (ludumGame);
        }
        if (release) {
            return new GameOverGameScreen (ludumGame);
        }
        if (analyze) {
            return new AnalyzeGameScreen (ludumGame);
        }
        if(escape) {
            return new MenuGameScreen(ludumGame);
        }
        return null;
    }

    @Override
    public boolean keyDown (int keycode) {

        if(keycode == Input.Keys.ESCAPE) {
            escape = true;
        }

        return false;
    }

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped (char character) {
        return false;
    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {


        return false;
    }

    @Override
    public boolean touchUp (int screenX, int screenY, int pointer, int button) {

        Vector2 mouse = screenToMouse (screenX, screenY);

        if (backButtonRect.contains (mouse)) {
            back = true;
        } else if (releaseButtonRect.contains (mouse)) {

            GameState.getInstance ().releaseLast ();
            released = true;
            if (GameState.getInstance ().getLast ().isSuccessful) {
                release = true;
            }

        } else if (analyzeButtonRect.contains (mouse)) {

            GameState.getInstance ().analyze ();
            analyze = true;

        }

        return false;
    }

    @Override
    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved (int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        return false;
    }

}
