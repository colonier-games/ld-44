package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.Colors;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.chemistry.Molecule;
import games.colonier.ld44.logic.AnalysisItem;
import games.colonier.ld44.logic.GameState;
import games.colonier.ld44.logic.TestCase;
import games.colonier.ld44.renderer.AtomsRenderer;
import games.colonier.ld44.text.Fonts;

import java.util.List;

public class AnalyzeGameScreen extends AbstractGameScreen implements InputProcessor {

    private AtomsRenderer atomsRenderer = new AtomsRenderer ();

    private Rectangle reactedMoleculeRect = new Rectangle ();
    private Rectangle symptomAnalysisRect = new Rectangle ();
    private Rectangle currentAnalysisItemRect = new Rectangle ();
    private Rectangle backButton = new Rectangle ();

    private boolean back = false;
    private boolean escape;

    public AnalyzeGameScreen (LudumGame ludumGame) {
        super (ludumGame);
    }

    @Override
    protected void onInit () {

        Gdx.input.setInputProcessor (this);

    }

    @Override
    protected void onResize (float w, float h) {

    }

    @Override
    protected void update (float delta) {

    }

    @Override
    protected void onDispose () {

    }

    @Override
    public void render (float delta) {

        Gdx.gl.glClearColor (
                Colors.Background.r,
                Colors.Background.g,
                Colors.Background.b,
                Colors.Background.a
        );
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT | Gdx.gl.GL_DEPTH_BUFFER_BIT);

        reactedMoleculeRect.set (
                32f, 32f,
                resolution.x / 2f - 16f - 32f, resolution.y - 64f
        );
        symptomAnalysisRect.set (
                resolution.x / 2f + 16f, 32f,
                resolution.x / 2f - 16f - 32f, resolution.y - 128f
        );
        backButton.set (
                resolution.x / 2f - 128f,
                32f,
                256f,
                64f
        );

        TestCase tc = GameState.getInstance ().getLastSimulated ();
        Molecule m = tc.reactedDiseaseMolecule;
        List<AnalysisItem> analysisItems = tc.analysisItems;

        if (m.atoms.isEmpty ()) {

            spriteBatch.begin ();

            Fonts.f96 ()
                    .setColor (Colors.TextLight);
            Fonts.f96 ()
                    .draw (
                            spriteBatch,
                            "No atoms remaining",
                            reactedMoleculeRect.x,
                            reactedMoleculeRect.y + reactedMoleculeRect.height - 64f,
                            reactedMoleculeRect.width,
                            Align.center,
                            true
                    );

            spriteBatch.end ();

        } else {

            spriteBatch.begin ();

            Fonts.f32 ()
                    .setColor (Colors.TextLight);
            Fonts.f32 ()
                    .draw (
                            spriteBatch,
                            "Product of reaction",
                            reactedMoleculeRect.x,
                            reactedMoleculeRect.y + reactedMoleculeRect.height,
                            reactedMoleculeRect.width,
                            Align.center,
                            true
                    );

            spriteBatch.end ();

            atomsRenderer.renderAtoms (
                    shapeRenderer,
                    m.atoms,
                    true,
                    reactedMoleculeRect,
                    12f
            );

        }

        if (!analysisItems.isEmpty ()) {

            spriteBatch.begin ();

            Fonts.f32 ()
                    .draw (
                            spriteBatch,
                            "Symptoms and causing structure",
                            symptomAnalysisRect.x,
                            reactedMoleculeRect.y + reactedMoleculeRect.height,
                            symptomAnalysisRect.width,
                            Align.center,
                            true
                    );


            for (int i = 0; i < analysisItems.size (); i++) {

                AnalysisItem item = analysisItems.get (i);

                currentAnalysisItemRect.set (
                        symptomAnalysisRect.x,
                        symptomAnalysisRect.y + symptomAnalysisRect.height - i * (128f + 48f),
                        symptomAnalysisRect.width,
                        128f + 48f
                );

                Fonts.f18 ().setColor (Colors.TextLight);
                Fonts.f18 ().draw (
                        spriteBatch,
                        item.symptom.name () + " (" + item.symptom.severity + ")",
                        currentAnalysisItemRect.x, currentAnalysisItemRect.y,
                        currentAnalysisItemRect.width,
                        Align.center,
                        true
                );

            }

            spriteBatch.end ();

            for (int i = 0; i < analysisItems.size (); i++) {

                AnalysisItem item = analysisItems.get (i);

                currentAnalysisItemRect.set (
                        symptomAnalysisRect.x,
                        symptomAnalysisRect.y + symptomAnalysisRect.height - 128f - 32f - i * (128f + 48f),
                        symptomAnalysisRect.width,
                        128f
                );

                atomsRenderer.renderAtoms (shapeRenderer, item.atoms, true, currentAnalysisItemRect, 10.0f);

                shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);
                shapeRenderer.line (
                        currentAnalysisItemRect.x,
                        currentAnalysisItemRect.y,
                        currentAnalysisItemRect.x + currentAnalysisItemRect.width,
                        currentAnalysisItemRect.y
                );
                shapeRenderer.end ();

            }

        } else {

            spriteBatch.begin ();

            Fonts.f96 ().setColor (Colors.TextLight);
            Fonts.f96 ().draw (
                    spriteBatch,
                    "Analysis shows no symptoms",
                    symptomAnalysisRect.x,
                    symptomAnalysisRect.y + symptomAnalysisRect.height - 64f,
                    symptomAnalysisRect.width,
                    Align.center,
                    true
            );

            spriteBatch.end ();

        }

        shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor (Colors.DarkGray);
        shapeRenderer.rect (
                backButton.x, backButton.y,
                backButton.width, backButton.height
        );

        shapeRenderer.end ();

        spriteBatch.begin ();

        Fonts.f32 ().setColor (Colors.TextLight);
        Fonts.f32 ().draw (
                spriteBatch,
                "Back",
                backButton.x, backButton.y + backButton.height / 2f,
                backButton.width,
                Align.center,
                true
        );

        spriteBatch.end ();

    }

    @Override
    public GameScreen next () {
        if (back) {
            return new TestResultsGameScreen (ludumGame);
        }
        if(escape) {
            return new MenuGameScreen(ludumGame);
        }
        return null;
    }

    @Override
    public boolean keyDown (int keycode) {

        if(keycode == Input.Keys.ESCAPE) {
            escape = true;
        }

        return false;
    }

    @Override
    public boolean keyUp (int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped (char character) {
        return false;
    }

    @Override
    public boolean touchDown (int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp (int screenX, int screenY, int pointer, int button) {

        Vector2 mouse = screenToMouse (screenX, screenY);

        if (backButton.contains (mouse)) {
            back = true;
        }

        return false;
    }

    @Override
    public boolean touchDragged (int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved (int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled (int amount) {
        return false;
    }

}
