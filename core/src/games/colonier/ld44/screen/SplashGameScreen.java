package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.text.Fonts;

public class SplashGameScreen extends AbstractGameScreen {

    private Color color = new Color (1f, 1f, 1f, 0f);

    public SplashGameScreen (LudumGame ludumGame) {
        super (ludumGame);
    }

    @Override
    protected void onInit () {

    }

    @Override
    protected void onResize (float w, float h) {

    }

    @Override
    protected void update (float delta) {

    }

    @Override
    protected void onDispose () {

    }

    @Override
    public void render (float delta) {

        Gdx.gl.glClearColor (0f, 0f, 0f, 1f);
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT);

        spriteBatch.begin ();

        spriteBatch.enableBlending ();

        Fonts.f32 ().setColor (
                color.set (1f, 1f, 1f, MathUtils.clamp (MathUtils.sin (getScreenTime () * (float) Math.PI / 3f) * 1.5f, 0.0f, 1.0f))
        );
        Fonts.f32 ().draw (spriteBatch, "Colonier Games presents ...", 0, resolution.y / 2f, resolution.x, Align.center, true);

        spriteBatch.disableBlending ();

        spriteBatch.end ();

    }

    @Override
    public GameScreen next () {
        if (getScreenTime () >= 3.0f) {
            return new MenuGameScreen (ludumGame);
        }
        return null;
    }

}
