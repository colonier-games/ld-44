package games.colonier.ld44.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.Colors;
import games.colonier.ld44.GameScreen;
import games.colonier.ld44.LudumGame;
import games.colonier.ld44.logic.GameState;
import games.colonier.ld44.text.Fonts;

public class CreditGameScreen extends AbstractGameScreen {

    private float increm;

    public CreditGameScreen(LudumGame ludumGame) {
        super(ludumGame);
    }

    @Override
    protected void onInit() {
        GameState.getInstance().setFirstRun(false);
    }

    @Override
    protected void onResize(float w, float h) {

    }

    @Override
    protected void update(float delta) {
        increm += 300.0f * delta;
    }

    @Override
    protected void onDispose () {

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor (
                Colors.Background.r,
                Colors.Background.g,
                Colors.Background.b,
                Colors.Background.a
        );
        Gdx.gl.glClear (Gdx.gl.GL_COLOR_BUFFER_BIT | Gdx.gl.GL_DEPTH_BUFFER_BIT);

        spriteBatch.begin ();

        Fonts.f128 ().draw (
                spriteBatch,
                "Credits",
                32f, -100.0f +  increm,
                resolution.x - 64,
                Align.center, true
        );

        Fonts.f64().draw(
                spriteBatch,
                "ColonierGames\n\nKovacs Botond Janos\nEros Akos",
                32f, - 100.0f - 160f + increm,
                resolution.x - 64,
                Align.center, true
        );

        spriteBatch.end ();
    }

    @Override
    public GameScreen next() {
        if(getScreenTime() >= 4.2f) {
            return new MenuGameScreen(ludumGame);
        }

        return null;
    }
}
