package games.colonier.ld44.music;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public final class Musics {

    private static Musics INSTANCE = new Musics ();

    private Music music;

    public static Musics getInstance () {
        return INSTANCE;
    }

    public void load () {
        music = Gdx.audio.newMusic (Gdx.files.internal ("sounds/music-1.wav"));
    }

    public void play () {
        music.play ();
    }

    public void pause () {
        music.pause ();
    }

    public void stop () {
        music.stop ();
    }

    public void dispose () {
        music.dispose ();
    }

    public void setLooping (boolean isLooping) {
        music.setLooping (isLooping);
    }

    public void setPlay (boolean isPlaying) {
        if (isPlaying) {
            music.play ();
        } else {
            music.pause ();
        }
    }

    public boolean isPlaying () {
        return music.isPlaying ();
    }

}
