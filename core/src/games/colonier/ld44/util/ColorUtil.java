package games.colonier.ld44.util;

import com.badlogic.gdx.graphics.Color;

public final class ColorUtil {

    private ColorUtil () { throw new UnsupportedOperationException (); }

    public static final String markup (Color gdxColor) {

        StringBuilder sb = new StringBuilder ();

        sb.append ("[#");
        sb.append (Integer.toHexString ((int) (gdxColor.r * 255.0f)));
        sb.append (Integer.toHexString ((int) (gdxColor.g * 255.0f)));
        sb.append (Integer.toHexString ((int) (gdxColor.b * 255.0f)));
        sb.append (Integer.toHexString ((int) (gdxColor.a * 255.0f)));
        sb.append ("]");

        return sb.toString ();

    }

}
