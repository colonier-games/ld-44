package games.colonier.ld44.text;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public final class Fonts {

    private static Fonts INSTANCE = new Fonts ();
    private BitmapFont font12;
    private BitmapFont font18;
    private BitmapFont font32;
    private BitmapFont font64;
    private BitmapFont font96;
    private BitmapFont font128;

    private Fonts () {
    }

    public static Fonts getInstance () {
        return INSTANCE;
    }

    public static BitmapFont f12 () {
        return INSTANCE.font12;
    }

    public static BitmapFont f18 () {
        return INSTANCE.font18;
    }

    public static BitmapFont f32 () {
        return INSTANCE.font32;
    }

    public static BitmapFont f64 () {
        return INSTANCE.font64;
    }

    public static BitmapFont f96 () {
        return INSTANCE.font96;
    }

    public static BitmapFont f128 () {
        return INSTANCE.font128;
    }

    public void load () {

        font12 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-12/font.fnt"));
        font18 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-18/font.fnt"));
        font32 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-32/font.fnt"));
        font64 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-64/font.fnt"));
        font96 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-96/font.fnt"));
        font128 = new BitmapFont (Gdx.files.internal ("fonts/disposable-droid-bb-128/font.fnt"));

        font12.getData ().markupEnabled = true;
        font18.getData ().markupEnabled = true;
        font32.getData ().markupEnabled = true;
        font64.getData ().markupEnabled = true;
        font96.getData ().markupEnabled = true;
        font128.getData ().markupEnabled = true;

    }

    public void dispose () {

        font12.dispose ();
        font18.dispose ();
        font32.dispose ();
        font64.dispose ();
        font96.dispose ();
        font128.dispose ();

    }

}
