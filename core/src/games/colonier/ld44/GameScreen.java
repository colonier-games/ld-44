package games.colonier.ld44;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public interface GameScreen {

    AssetManager getAssetManager ();

    SpriteBatch getSpriteBatch ();

    ShapeRenderer getShapeRenderer ();

    float getScreenTime ();

    float getGameTime ();

    float getDelta ();

    default Vector2 getResolution () {
        return new Vector2 (Gdx.graphics.getWidth (), Gdx.graphics.getHeight ());
    }

    void init ();

    void tick (float delta);

    void render (float delta);

    void resize (float w, float h);

    void dispose ();

    GameScreen next ();

}
