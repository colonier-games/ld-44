package games.colonier.ld44;

import com.badlogic.gdx.graphics.Color;

public final class Colors {

    private Colors () { throw new UnsupportedOperationException (); }

    public static final Color AtomRed = new Color (248.0f / 255.0f, 82.0f / 255.0f, 82.0f / 255.0f, 1.0f);
    public static final Color AtomRedDark = new Color (128.0f / 255.0f, 42.0f / 255.0f, 42.0f / 255.0f, 1.0f);
    public static final Color AtomGreen = new Color (142.0f / 255.0f, 244.0f / 255.0f, 125.0f / 255.0f, 1.0f);
    public static final Color AtomGreenDark = new Color (73.0f / 255.0f, 128.0f / 255.0f, 65.0f / 255.0f, 1.0f);
    public static final Color AtomBlue = new Color (67.0f / 255.0f, 161.0f / 255.0f, 233.0f / 255.0f, 1.0f);
    public static final Color AtomBlueDark = new Color (37.0f / 255.0f, 88.0f / 255.0f, 128.0f / 255.0f, 1.0f);
    public static final Color Background = new Color (221.0f / 255.0f, 221.0f / 255.0f, 207.0f / 255.0f, 1.0f);
    public static final Color LightPanelBackground = new Color (248.0f / 255.0f, 248.0f / 255.0f, 233.0f / 255.0f, 1.0f);
    public static final Color LightPanelShadow = new Color (131.0f / 255.0f, 131.0f / 255.0f, 123.0f / 255.0f, 1.0f);
    public static final Color DarkGray = new Color (76.0f / 255.0f, 76.0f / 255.0f, 71.0f / 255.0f, 1.0f);
    public static final Color LightGray = new Color (197.0f / 255.0f, 190.0f / 255.0f, 180.0f / 255.0f, 1.0f);
    public static final Color TextDark = new Color (143.0f / 255.0f, 117.0f / 255.0f, 117.0f / 255.0f, 1.0f);
    public static final Color TextLight = new Color (248.0f / 255.0f, 248.0f / 255.0f, 233.0f / 255.0f, 1.0f);
    public static final Color Green = new Color (94.0f / 255.0f, 198.0f / 255.0f, 84.0f / 255.0f, 1.0f);
    public static final Color BluePanelBackground = new Color(211f / 255f, 226f / 255f,225f / 255f,1.0f);
    public static final Color Orange = new Color (226.0f / 255.0f, 175.0f / 255.0f, 56.0f / 255.0f, 1.0f);

}
