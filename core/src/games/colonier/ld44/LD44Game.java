package games.colonier.ld44;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import games.colonier.ld44.chemistry.rule.RuleGenerator;
import games.colonier.ld44.chemistry.rule.RuleTextifier;
import games.colonier.ld44.logic.GameState;
import games.colonier.ld44.music.Musics;
import games.colonier.ld44.screen.MenuGameScreen;
import games.colonier.ld44.screen.MoleculeEditorGameScreen;
import games.colonier.ld44.screen.SplashGameScreen;
import games.colonier.ld44.text.Fonts;

public class LD44Game extends ApplicationAdapter implements LudumGame {

    GameScreen gameScreen;
    Music music;
    private float appTime = 0.0f;

    @Override
    public void create () {

        Fonts.getInstance ().load ();
        Musics.getInstance ().load ();
        Musics.getInstance ().setLooping (true);
        Musics.getInstance ().play ();
        GameState.getInstance().load();

        System.out.println (RuleTextifier.textify (RuleGenerator.generate (5)));

        gameScreen = new SplashGameScreen (this);
        gameScreen.init ();

    }

    @Override
    public void render () {
        Gdx.gl.glClearColor (0, 0, 0, 1);
        Gdx.gl.glClear (GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        float delta = Gdx.graphics.getDeltaTime ();
        appTime += delta;

        gameScreen.tick (delta);
        gameScreen.render (delta);

        GameScreen next = gameScreen.next ();
        if (next != null) {
            gameScreen.dispose ();
            gameScreen = next;
            gameScreen.init ();
        }

    }

    @Override
    public void dispose () {

        Fonts.getInstance ().dispose ();
        Musics.getInstance ().dispose ();

        if (gameScreen != null) {
            gameScreen.dispose ();
        }

    }

    @Override
    public void resize (int width, int height) {
        super.resize (width, height);

        gameScreen.resize (width, height);

    }

    @Override
    public float getAppTime () {
        return appTime;
    }

}
