package games.colonier.ld44.renderer;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import games.colonier.ld44.Colors;
import games.colonier.ld44.text.Fonts;

import java.util.ArrayList;
import java.util.List;

public class TutorialRenderer {

    public final List<String> texts = new ArrayList<> ();
    public int index = 0;
    public boolean finished = false;

    private Rectangle dialogRect = new Rectangle ();
    private Rectangle dialogInsideRect = new Rectangle ();
    private Rectangle skipButtonRect = new Rectangle ();
    private Rectangle nextButtonRect = new Rectangle ();
    private Color dialogBackground = new Color (
            Colors.DarkGray.r, Colors.DarkGray.g, Colors.DarkGray.b, 0.4f
    );

    public void click (Vector2 mouse) {

        if (skipButtonRect.contains (mouse)) {
            index = texts.size ();
        } else if (nextButtonRect.contains (mouse)) {
            index++;
        }

        if (index >= texts.size ()) {
            finished = true;
        }

    }

    public void render (
            ShapeRenderer shapeRenderer,
            SpriteBatch spriteBatch,
            float delta,
            Vector2 resolution
    ) {

        dialogRect.set (
                128f, 128f, resolution.x - 256f, resolution.y - 256f
        );
        dialogInsideRect.set (
                dialogRect.x + 32f, dialogRect.y + 32f, dialogRect.width - 64f, dialogRect.height - 64f
        );
        skipButtonRect.set (
                dialogInsideRect.x, dialogInsideRect.y, 256f, 64f
        );
        nextButtonRect.set (
                dialogInsideRect.x + dialogInsideRect.width - 256f,
                dialogInsideRect.y,
                256f, 64f
        );

        shapeRenderer.begin (ShapeRenderer.ShapeType.Filled);

        shapeRenderer.setColor (dialogBackground);
        shapeRenderer.rect (
                dialogRect.x, dialogRect.y,
                dialogRect.width, dialogRect.height
        );

        shapeRenderer.setColor (Colors.LightGray);
        shapeRenderer.rect (
                skipButtonRect.x, skipButtonRect.y, skipButtonRect.width, skipButtonRect.height
        );

        shapeRenderer.setColor (Colors.Green);
        shapeRenderer.rect (
                nextButtonRect.x, nextButtonRect.y, nextButtonRect.width, nextButtonRect.height
        );

        shapeRenderer.end ();


        spriteBatch.begin ();

        Fonts.f32 ().setColor (Colors.TextLight);
        Fonts.f32 ().draw (
                spriteBatch,
                index <= texts.size () - 1 ? texts.get (index) : "",
                dialogInsideRect.x, dialogInsideRect.y + dialogInsideRect.height,
                dialogInsideRect.width, Align.topLeft, true
        );

        Fonts.f64 ().setColor (Colors.TextLight);
        Fonts.f64 ()
                .draw (spriteBatch,
                        "Skip",
                        skipButtonRect.x,
                        skipButtonRect.y + skipButtonRect.height / 2f + 16f,
                        skipButtonRect.width,
                        Align.center,
                        true
                );

        Fonts.f64 ()
                .draw (spriteBatch,
                        "Next",
                        nextButtonRect.x,
                        nextButtonRect.y + nextButtonRect.height / 2f + 16f,
                        nextButtonRect.width,
                        Align.center,
                        true
                );

        spriteBatch.end ();

    }

}
