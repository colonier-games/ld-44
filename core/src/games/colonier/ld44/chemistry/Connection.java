package games.colonier.ld44.chemistry;

import java.util.Objects;

public final class Connection {

    public int id1;
    public int id2;

    public Connection (int id1, int id2) {
        this.id1 = id1;
        this.id2 = id2;
        if (this.id1 > this.id2) {
            int tmp = this.id2;
            this.id2 = this.id1;
            this.id1 = tmp;
        }
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass () != o.getClass ()) {
            return false;
        }
        Connection that = (Connection) o;
        return id1 == that.id1 &&
                id2 == that.id2;
    }

    @Override
    public int hashCode () {
        return Objects.hash (id1, id2);
    }

}
