package games.colonier.ld44.chemistry.rule;

import games.colonier.ld44.chemistry.Atom;
import games.colonier.ld44.chemistry.Molecule;
import games.colonier.ld44.chemistry.MoleculeRule;

public class StatisticalMoleculeRule implements MoleculeRule {

    public final int red, green, blue;
    public final int redRed, redGreen, redBlue, greenGreen, greenBlue, blueBlue;

    public StatisticalMoleculeRule (
            int red,
            int green,
            int blue,
            int redRed,
            int redGreen,
            int redBlue,
            int greenGreen,
            int greenBlue,
            int blueBlue
    ) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.redRed = redRed;
        this.redGreen = redGreen;
        this.redBlue = redBlue;
        this.greenGreen = greenGreen;
        this.greenBlue = greenBlue;
        this.blueBlue = blueBlue;
    }


    @Override
    public boolean appliesTo (Molecule molecule) {

        int mRedCount = molecule.atomCount (Atom.Type.Red);
        int mGreenCount = molecule.atomCount (Atom.Type.Green);
        int mBlueCount = molecule.atomCount (Atom.Type.Blue);

        if (red > 0 && mRedCount != red) return false;
        if (green > 0 && mGreenCount != green) return false;
        if (blue > 0 && mBlueCount != blue) return false;

        int mRRCount = molecule.connectionCount (Atom.Type.Red, Atom.Type.Red);
        int mRGCount = molecule.connectionCount (Atom.Type.Red, Atom.Type.Green);
        int mRBCount = molecule.connectionCount (Atom.Type.Red, Atom.Type.Blue);
        int mGGCount = molecule.connectionCount (Atom.Type.Green, Atom.Type.Green);
        int mGBCount = molecule.connectionCount (Atom.Type.Green, Atom.Type.Blue);
        int mBBCount = molecule.connectionCount (Atom.Type.Blue, Atom.Type.Blue);

        if (redRed > 0 && mRRCount != redRed) return false;
        if (redGreen > 0 && mRGCount != redGreen) return false;
        if (redBlue > 0 && mRBCount != redBlue) return false;
        if (greenGreen > 0 && mGGCount != greenGreen) return false;
        if (greenBlue > 0 && mGBCount != greenBlue) return false;
        if (blueBlue > 0 && mBBCount != blueBlue) return false;

        return true;

    }

}
