package games.colonier.ld44.chemistry;

import games.colonier.ld44.chemistry.rule.StructuralMolecularRule;

public enum Symptom {

    Headache (
            1, new StructuralMolecularRule ("bggrrggbbr")
    ),
    Nausea (
            1, new StructuralMolecularRule ("grrggggggr")
    ),
    Dyspepsia (
            1, new StructuralMolecularRule ("bggbbrrbbg")
    ),
    KidneyFailure (
            3, new StructuralMolecularRule ("gbbbbbbrrg")
    ),
    LiverDamage (
            3,
            new StructuralMolecularRule ("gbbrrggbbb")
    ),
    Death (
            6,
            new StructuralMolecularRule ("rrrggggbbb")
    ),
    Rashes (
            2, new StructuralMolecularRule ("gbbrrggbbb")
    ),
    HairLoss (
            3, new StructuralMolecularRule ("rbbggggrrb")
    ),
    Fever (
            2,
            new StructuralMolecularRule ("rggrrggggr")
    ),
    VisionDamage (
            4,
            new StructuralMolecularRule ("bbbrrggbbg")
    ),
    Choking (
            4, new StructuralMolecularRule ("bggrrggrrb")
    ),
    HearingDamage (
            4, new StructuralMolecularRule ("gbbggggggb")
    );

    public final int severity;
    public final StructuralMolecularRule rule;

    Symptom (int severity, StructuralMolecularRule rule) {
        this.severity = severity;
        this.rule = rule;
    }
}
