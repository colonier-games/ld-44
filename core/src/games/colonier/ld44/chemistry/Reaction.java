package games.colonier.ld44.chemistry;

import games.colonier.ld44.chemistry.reaction.ReactionPattern;

import java.util.Arrays;
import java.util.List;

public class Reaction {

    private static final List<ReactionPattern> REACTION_PATTERNS = Arrays.asList (
            new ReactionPattern ("RR", "GB"),
            new ReactionPattern ("RG", "BB"),
            new ReactionPattern ("RB", "RR"),
            new ReactionPattern ("GG", "RG"),
            new ReactionPattern ("GB", "RB"),
            new ReactionPattern ("BB", "GG")
    );

    public final Molecule disease;
    public final Molecule medicine;

    public Reaction (Molecule disease, Molecule medicine) {
        this.disease = disease;
        this.medicine = medicine;
    }

    public void react () {

        for (ReactionPattern rp : REACTION_PATTERNS) {
            rp.react (medicine, disease);
        }

        int dLastId = 1;
        if (!disease.atoms.isEmpty ()) {
            dLastId = disease.atoms.stream ().mapToInt (a -> a.id).max ().getAsInt () + 1;
        }

        int i = 0;
        for (Atom atom : medicine.atoms) {
            atom.id = dLastId + i++;
            disease.atoms.add (atom);
        }

        disease.arrangeAtoms ();

    }

}
