package games.colonier.ld44.chemistry;

@FunctionalInterface
public interface MoleculeRule {

    boolean appliesTo (Molecule molecule);

}
