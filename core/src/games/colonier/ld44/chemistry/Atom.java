package games.colonier.ld44.chemistry;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import games.colonier.ld44.Colors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Atom implements Serializable {

    public int id;
    public Type type;
    public List<Atom> connectedTo = new ArrayList<> ();
    public Vector2 drawPosition = new Vector2 ();
    public Atom (int id, Type type) {
        this.id = id;
        this.type = type;
    }

    public Atom connect (Atom other) {
        if (!connectedTo.contains (other)) {
            connectedTo.add (other);
        }
        if (!other.connectedTo.contains (this)) {
            other.connect (this);
        }
        return this;
    }

    public Atom disconnect (Atom other) {
        if(connectedTo.contains(other)) {
            connectedTo.remove(other);
        }
        if(other.connectedTo.contains(this)) {
            other.disconnect(this);
        }
        return this;
    }

    public Atom disconnectAll () {
        while(!connectedTo.isEmpty()) {
            disconnect(connectedTo.get(0));
        }
        return this;
    }

    public boolean isConnected(Atom other) {
        return connectedTo.contains(other);
    }

    public Atom drawPosition (float x, float y) {
        this.drawPosition.set (x, y);
        return this;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass () != o.getClass ()) {
            return false;
        }
        Atom atom = (Atom) o;
        return id == atom.id &&
                type == atom.type;
    }

    @Override
    public int hashCode () {
        return Objects.hash (id, type);
    }

    public enum Type {
        Red (Colors.AtomRed),
        Green (Colors.AtomGreen),
        Blue (Colors.AtomBlue);

        public final Color color;

        Type (Color color) {
            this.color = color;
        }
    }

    public boolean isRed () { return type == Type.Red; }
    public boolean isGreen () { return type == Type.Green; }
    public boolean isBlue () { return type == Type.Blue; }

}
