package games.colonier.ld44.menu;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;

import java.util.ArrayList;

public class ButtonList {

    private ArrayList<Button> buttonsList = new ArrayList<>();
    private Button currentButton;
    public int pos;
    private Stage stage;

    public ButtonList(Stage stage, Button... buttons) {
        int tempPos = 1;
        for (Button b: buttons) {
            buttonsList.add(b);
        }
        currentButton = buttonsList.get(0);
        pos = 0;
        this.stage = stage;

        for(Button b : buttonsList) {
            this.setButton(tempPos++, b);
            this.stage.addActor(b);
        }

    }

    public ArrayList<Button> getButtonsList() {
        return buttonsList;
    }

    public Button getCurrentButton() {
        return currentButton;
    }

    public void setCurrentButton(Button currentButton) {
        this.currentButton = currentButton;
    }

    public void incremPosition () {
        if(this.buttonsList.size() - 1 != pos) {
            this.pos++;
            this.currentButton = buttonsList.get(pos);
            /* this.currentButton = buttonsList.get(pos);
            for ( Button b : this.buttonsList) {
                setButton(buttonsList.indexOf(b) - pos , b);
            } */
        }
    }

    public void decremPosition () {
        if(pos != 0) {
            this.pos--;
            this.currentButton = buttonsList.get(pos);
            /* this.currentButton = buttonsList.get(pos);
            for ( Button b : this.buttonsList) {
                setButton(buttonsList.indexOf(b) - pos , b);
            } */
        }
    }

    public void setButton(int position, Button button) {
        button.setPosition(
                (this.stage.getViewport().getScreenWidth() / 2.0f) - (button.getWidth() / 2.0f),
                (this.stage.getViewport().getScreenHeight() / 2.0f)
                        - (button.getHeight() * position * 1.1f) + 32f
        );
    }

}
