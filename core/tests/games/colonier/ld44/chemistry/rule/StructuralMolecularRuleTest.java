package games.colonier.ld44.chemistry.rule;

import games.colonier.ld44.chemistry.Atom;
import games.colonier.ld44.chemistry.Molecule;
import games.colonier.ld44.chemistry.MoleculeRule;
import games.colonier.ld44.chemistry.Symptom;
import org.junit.Test;

import static org.junit.Assert.*;

public class StructuralMolecularRuleTest {

    @Test
    public void test () {

        Atom red = new Atom (1, Atom.Type.Red);
        Atom green = new Atom (2, Atom.Type.Green);
        Atom blue = new Atom (3, Atom.Type.Blue);

        red.connect (green);
        green.connect (blue);
        blue.connect (red);

        Molecule m = new Molecule (1)
                .atom (red).atom (green).atom (blue);

        MoleculeRule mr = new StructuralMolecularRule ("RGB");

        assertTrue (mr.appliesTo (m));

    }

    @Test
    public void testHeadacheTrue () {

        Atom red = new Atom (1, Atom.Type.Red);
        Atom green = new Atom (2, Atom.Type.Green);
        Atom blue1 = new Atom (3, Atom.Type.Blue);
        Atom blue2 = new Atom (4, Atom.Type.Blue);

        red.connect (green).connect (blue1);
        green.connect (blue2).connect (blue1);
        blue1.connect (blue2);

        Molecule m = new Molecule (0)
                .atom (red).atom (green).atom (blue1).atom (blue2);

        assertTrue (
                Symptom.Headache.rule.appliesTo (m)
        );

    }

    @Test
    public void testHeadacheFalse () {

        Atom red = new Atom (1, Atom.Type.Red);
        Atom green = new Atom (2, Atom.Type.Green);
        Atom blue1 = new Atom (3, Atom.Type.Blue);
        Atom blue2 = new Atom (4, Atom.Type.Blue);

        red.connect (green).connect (blue1);
        green.connect (blue2).connect (blue1);

        Molecule m = new Molecule (0)
                .atom (red).atom (green).atom (blue1).atom (blue2);

        assertFalse (
                Symptom.Headache.rule.appliesTo (m)
        );

    }

}