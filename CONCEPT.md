# Ludum Dare 44 - Concept

## Gyógyszer fejlesztés

A játék során egy véletlenszerűen generált betegség ellenszerét kell kutatni. Az ellenszer csak
akkor kerülhet piacra, ha a gyógyszer mellékhatásai elfogadható határokon belül vannak. A játékos
feladata, hogy az egyes atomokat összekapcsolva molekulákat alkosson. A molekulákra vonatkoznak
szabályok; bizonyos kombinációk okozzák a gyógyszerek mellékhatásait. A betegségnek van egy 
ismert gyengesége, melyet kihasználva elkészíthető a gyógyszer.

Minden mellékhatásnak van egy súlyossága. A gyógyszer súlyossága a mellékhatások súlyossági
pontszámainak összege. Amennyiben ez az összeg meghaladja a hatot, a gyógyszer nem dobható
piacra.

A lehetséges mellékhatások:

* Fejfájás (1 pont)
* Hányinger (1 pont)
* Gyomorbaj (1 pont)
* Vese leállás (3 pont)
* Májkárosodás (3 pont)
* Kiütések (2 pont)
* Hajhullás (3 pont)
* Láz (2 pont)
* Látáskárosodás (4 pont)
* Halláskárosodás (4 pont)
* Fulladás (4 pont)
* Halál (6 pont)

A játékban a kémiai szabályok nem felelnek meg a valóságnak. A virtuális kémiában 3 féle atom
van: piros, zöld és kék atom. Az egyes atomok bármennyi másik atommal összekötődhetnek.

A betegség leírása határozza meg, hogy milyen felépítésű molekulát szükséges összeállítani a 
legyőzéséhez. Ez a leírás olyan szabályokat tartalmaz, mint "páros számú piros atom", stb.
Ezen szabályok lehetséges elemei:

* n darab X atom
* Páros/Páratlan számú X atom
* k-szor annyi Y atom, mint X atom
* k-val több/kevesebb Y atom, mint X atom
* Legfeljebb/legalább n darab X atom
* n darab X-Y atomi kötés
* Páros/Páratlan számú X-Y atomi kötés
* k-szor annyi X-Y atomi kötés, mint Z-W atomi kötés
* k-val több/kevesebb X-Y atomi kötés, mint Z-W atomi kötés
* Legfeljebb/legalább n darab X-Y atomi kötés

Példa betegségleírás:

```
A betegség legyőzhető olyan molekulával, melyben páros számú PIROS atom, 3-mal több ZÖLD atom,
mint PIROS atom, és páratlan számú ZÖLD-KÉK atomi kötés található.
```

A játékos célja, hogy a lehető legkevesebb pénzt felhasználva fejlessze ki a lehető legkevesebb
mellékhatást rejtő, a betegséget kezelő gyógyszert. Ha a játékos pénze elfogy, akkor veszít.