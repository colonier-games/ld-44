import math, random

alphabet = ['r', 'g', 'b']
length = 5

result = []
num_results = 12

for i in range (num_results):
    r = []
    l = random.choice (alphabet)
    for j in range (length):
        c = random.choice (alphabet)
        r.append (l + c)
        l = c
    result.append (''.join(r))
    
for r in result:
    print (r)
